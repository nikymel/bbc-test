const uc = require('upper-case');

// Module to populate the page with JSON file data
exports.getJSONComponents = function (json_data, res) {
    console.log('\n[populate.js] Populating page');
    res.write('\n<h1>' + uc(json_data.title) + '</h1>\n');

    // Loop through all body elements of JSON file
    json_data.body.forEach(function(part) {
        console.log('[populate.js] Adding ' + part.type);
        if (part.type === 'heading') {
            res.write('<h3>' + part.model.text + '</h3>\n');
        }
        else if (part.type === 'paragraph') {
            res.write('<p>' + part.model.text + '</p>\n');
        }
        else if (part.type === 'image') {
            res.write('<img src="' + part.model.url +
                '" alt="' + part.model.altText +
                '" style="height:' + part.model.height +
                ';width:' + part.model.width + '"></br>\n');
        }
        else if (part.type === 'list') {
            if (part.model.type === 'unordered') {
                res.write('<ul>\n');
            }
            else res.write('<ol>\n');
            part.model.items.forEach(function(element) {
                res.write('    <li>' + element + '</li>\n');
            });
            if (part.model.type === 'unordered') {
                res.write('</ul>\n');
            }
            else res.write('</ol>\n');
        }
    });
    res.write('\n</body>\n</html>');
};