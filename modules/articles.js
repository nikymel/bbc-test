// Module to get article based on integer input.
exports.getArticle = function (article) {
    if (article > 5 || article < 1) return -1;
    let article_choice = "../data/article-" + article.toString();
    if (article_choice === '../data/article-NaN') return -1;
    let article_data = require(article_choice);
    console.log('\n[articles.js] Retrieving article ' + article.toString());
    return article_data;
};
