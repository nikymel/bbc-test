// Setting server info

const http = require('http');
const hostname = '127.0.0.1';
const port = '8080';

const art = require('./modules/articles');
const populate = require('./modules/populate');

const fs = require('fs');
const uc = require('upper-case');
const url = require('url');
const express = require("express");

const server = http.createServer((req, res) => {
    let app = express();
    app.use(express.static('public'));
    res.statusCode = 200;
    let query = url.parse(req.url, true).query;

    let article_choice = art.getArticle(parseInt(query.article));
    if (article_choice === -1) {
        res.writeHead(200,{'Content-Type': 'text/html'});
        res.write('Article does not exist.');
        res.end();
    } else {
        fs.readFile('index.html', function(err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);

            populate.getJSONComponents(article_choice, res);

            res.end();
        });
    }
});

server.listen(port, hostname, () => {
    console.log('[main.js] Server running.');
});